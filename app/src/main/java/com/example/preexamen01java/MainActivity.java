package com.example.preexamen01java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

// Proyecto terminado

public class MainActivity extends AppCompatActivity {
    private Button btnEntrar;
    private Button btnSalir;
    private EditText txtNombreTrabajador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnEntrar= findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador);
    }

    private void ingresar() {
        String strUsuario;

        strUsuario = getResources().getString(R.string.nombre);

        if (strUsuario.equals(txtNombreTrabajador.getText().toString())) {
            // Crear un Bundle para enviar información
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreTrabajador.getText().toString());

            // Crear un intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando o no una respuesta
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Compruebe el nombre", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo Nómina");
        confirmar.setMessage("Salir de la app");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Cancelar, no se realiza ninguna acción
            }
        });
        confirmar.show();
    }
}