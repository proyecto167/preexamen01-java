package com.example.preexamen01java;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblNombreUsuario;
    private EditText txtNumRecibo;
    private EditText txtNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtras;
    private RadioButton rdbPuesto1;
    private RadioButton rdbPuesto2;
    private RadioButton rdbPuesto3;
    private EditText txtSubtotal;
    private EditText txtImpuesto;
    private EditText txtTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    // Declarar objeto
    ReciboNomina recibo = new ReciboNomina();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibonomina);
        iniciarComponentes();
        Bundle datos = getIntent().getExtras();
        if (datos != null) {
            String usuario = datos.getString("nombre");
            if (usuario != null) {
                lblNombreUsuario.setText(usuario);
            }
        }
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodoCalcular();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodoLimpiar();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodoRegresar();
            }
        });
    }

    private void iniciarComponentes() {
        // Asignar los atributos privados a los elementos del diseño
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        rdbPuesto1 = findViewById(R.id.rdbPuesto1);
        rdbPuesto2 = findViewById(R.id.rdbPuesto2);
        rdbPuesto3 = findViewById(R.id.rdbPuesto3);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotal = findViewById(R.id.txtTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblNombreUsuario= findViewById(R.id.lblNombreUsuario);

        // Inicializar la calculadora
        recibo = new ReciboNomina();
    }

    private void metodoCalcular(){
        // Puesto
        int puesto=0;
        float horasNormales=0;
        float horasExtras=0;
        float subtotal=0;
        float impuesto=0;
        float total=0;

        if (rdbPuesto1.isChecked()) {
            puesto = 1;
        } else if (rdbPuesto2.isChecked()) {
            puesto = 2;
        } else if (rdbPuesto3.isChecked()){
            puesto = 3;
        }

        if (txtNumRecibo.getText().toString().isEmpty()) {
            Toast.makeText(this, "Ingrese su Número de Recibo", Toast.LENGTH_SHORT).show();
        } else if (txtNombre.getText().toString().isEmpty()) {
            Toast.makeText(this, "Ingrese su Nombre", Toast.LENGTH_SHORT).show();
        } else if (txtHorasNormal.getText().toString().isEmpty() || txtHorasExtras.getText().toString().isEmpty()) {
            if (txtHorasNormal.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese la cantidad de Horas Trabajadas", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Ingrese la cantidad de Horas Extras Trabajadas", Toast.LENGTH_SHORT).show();
            }
        }

        // Horas trabajadas
        if (!txtHorasNormal.getText().toString().isEmpty()) {
            horasNormales = Float.parseFloat(txtHorasNormal.getText().toString());
        }
        if (!txtHorasExtras.getText().toString().isEmpty()) {
            horasExtras = Float.parseFloat(txtHorasExtras.getText().toString());
        }


        // setear valores a nomina
        recibo.setPuesto(puesto);
        recibo.setHorasTrabNormal(horasNormales);
        recibo.setHorasTrabExtras(horasExtras);

        // Llamar a los métodos
        if (!txtHorasNormal.getText().toString().isEmpty() && !txtHorasExtras.getText().toString().isEmpty() && puesto != 0){
            subtotal = recibo.CalcularSubtotal();
            impuesto = recibo.CalcularImpuesto(subtotal);
            total = recibo.CalcularTotal(subtotal, impuesto);
            txtSubtotal.setText(String.valueOf(subtotal));
            txtImpuesto.setText(String.valueOf(impuesto));
            txtTotal.setText(String.valueOf(total));
        }

    }

    private void metodoLimpiar(){
        txtNumRecibo.setText("");
        txtNombre.setText("");
        txtHorasNormal.setText("");
        txtHorasExtras.setText("");
        txtSubtotal.setText("");
        txtImpuesto.setText("");
        txtTotal.setText("");

        // Deseleccionar los RadioButtons
        rdbPuesto1.setChecked(true);
        rdbPuesto2.setChecked(false);
        rdbPuesto3.setChecked(false);
    }

    private void metodoRegresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Cálculo de Pago de Nómina");
        confirmar.setMessage("Regresar al inicio de sesión");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }




}
