package com.example.preexamen01java;

public class ReciboNomina {
    private int numRecibo;
    private String Nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    // Constructor
    public ReciboNomina() {

    }

    // Getter y Setter
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    // Métodos
    public float CalcularSubtotal(){
        float subtotal=0;
        float pagoBase=200;
        switch(this.getPuesto()){
            case 1:
                // Caso Auxiliar
                pagoBase=200+(200*20/100);
                subtotal=(pagoBase*this.getHorasTrabNormal())+(pagoBase*this.getHorasTrabExtras()*2);
                return subtotal;
            case 2:
                // Caso Albañil
                pagoBase=200+(200*50/100);
                subtotal=(pagoBase*this.getHorasTrabNormal())+(pagoBase*this.getHorasTrabExtras()*2);
                return subtotal;
            case 3:
                // Caso Ing. Obra
                pagoBase=200+(200);
                subtotal=(pagoBase*this.getHorasTrabNormal())+(pagoBase*this.getHorasTrabExtras()*2);
                return subtotal;
            default:
                return subtotal;
        }
    }

    public float CalcularImpuesto(float subtotal){
        float impuesto;
        impuesto=subtotal*16/100;
        return impuesto;
    }

    public float CalcularTotal(float subtotal, float impuesto){
        float total;
        total=subtotal-impuesto;
        return total;
    }




}
